package it.uniba.di.lacam.ml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
//import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
//import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
//import org.semanticweb.HermiT.Reasoner;
//import org.semanticweb.HermiT.Reasoner.*;

import uk.ac.manchester.cs.jfact.JFactFactory;


import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.expression.OWLEntityChecker;
import org.semanticweb.owlapi.expression.ShortFormEntityChecker;
import org.semanticweb.owlapi.manchestersyntax.renderer.ParserException;
import org.semanticweb.owlapi.util.BidirectionalShortFormProvider;
import org.semanticweb.owlapi.util.BidirectionalShortFormProviderAdapter;
import org.semanticweb.owlapi.util.ShortFormProvider;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;
import org.semanticweb.owlapi.util.mansyntax.ManchesterOWLSyntaxParser;





/**
 * Representation of a (supervised) learning problem: 
 * 		default settings for the parameters, 
 * 		ontology/reasoner management objects,
 * 
 * 		 
 * @author NF
 *
 */
public class LProblem {

//	Properties settings = new Properties();
	XMLConfiguration settings;	// non default
	
	int nRandTargetConcepts = -1; // > 0
	int nFolds = -1;
	int nCandSubConcepts = -1;
	
	int seed = -1;	// default seed for the random generator
	
	IRI iri;  // the ontology IRI
	String urlOwlFile;
	
	OWLOntologyManager manager;
	OWLDataFactory dataFactory;
	OWLOntology ontology;
	OWLReasoner reasoner;
	OWLReasonerFactory reasonerFactory = new JFactFactory();
	
	OWLNamedIndividual[] allIndividuals;
	OWLClass[] allConcepts;
	OWLObjectProperty[] allRoles;
	OWLDataProperty[] allDataProperties;

	OWLClassExpression[] testConcepts, negTestConcepts;	// concepts to be learned + their complements
//	OWLNamedIndividual[] trainingExamples; // training examples
	OWLClass top; // top concept (Thing)
	
//	ArrayList<Integer>[] positiveExs; // all positive exs
//	ArrayList<Integer>[] negativeExs;
//	ArrayList<Integer>[] unlabeledExs;

	int[][] classification; // deductive classification [c][i] w.r.t. c-th class of the i-th individual 
	
	Map<String, Class<?>> algoMap = new HashMap<String, Class<?>>();

	Class<?> algoType;

	
	/**
	 * @param clnArg
	 */
	@SuppressWarnings("deprecation")
	LProblem(String clnArg) {

		// create and load default properties
		
		Parameters params = new Parameters();
		FileBasedConfigurationBuilder<XMLConfiguration> builder =
			    new FileBasedConfigurationBuilder<XMLConfiguration>(XMLConfiguration.class).configure(params.xml().setFileName(clnArg));
		
//		FileInputStream in;
		try {
			settings = builder.getConfiguration();
//			in = new FileInputStream(clnArg);
//			settings.loadFromXML(in);			
//			in.close();
//		} catch (FileNotFoundException e) {
//			System.out.println("File Not Found:"+clnArg);
//			e.printStackTrace();
//		} catch (IOException e) {
//			System.out.println("I/O problem with "+clnArg);
		} catch (ConfigurationException ce) {
			System.out.println("Config file problem with "+clnArg);
			System.exit(1);
		}

//	    // trim off any excess whitespace from properties
//	    for (Object tmpKey : Collections.list(settings.propertyNames())) {
//	      String tmpValue = settings.getProperty((String) tmpKey);
//	      tmpValue = tmpValue.trim();
//	      settings.put(tmpKey, tmpValue);
//	    }
		
//		settings.list(System.out);
		
		
		
		System.out.println("INIT KB <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	
		
//		urlOwlFile = settings.getProperty("KB");
		urlOwlFile = settings.getString("kb");

		
		Locale.setDefault(Locale.US);

		iri = IRI.create(urlOwlFile);

		
		initKB();
		
				
		// read from config
		
//		seed = Integer.parseInt(settings.getProperty("SEED"));
		seed = settings.getInt("seed");

//		nRandTargetConcepts = Integer.parseInt(settings.getProperty("NT"));
		nRandTargetConcepts = settings.getInt("evaluation.NRT");
		
//		nFolds = Integer.parseInt(settings.getProperty("NF"));
		nFolds = settings.getInt("evaluation.NF");

//		nCandSubConcepts = Integer.parseInt(settings.getProperty("NC"));
		nCandSubConcepts = settings.getInt("algo.NC");

// 		TODO abs algo
		String algoTypeStr = settings.getString("algo.type");
		if (algoTypeStr != null) {
			algoMap.put("DLFoil", CEClassificationModel.class);
			algoMap.put("TDT", TDTClassificationModel.class);
			
			algoType = algoMap.get(algoTypeStr);
		}
		else 
			algoType = CEClassificationModel.class;
		
//		String targetClass = settings.getProperty("TARGET");
		
		List<Object> targets = settings.getList("evaluation.targets.target");
		
		if (targets!=null) {
			
	        nRandTargetConcepts = targets.size();

	        testConcepts = new OWLClassExpression[nRandTargetConcepts];
			// read and convert list of target class expressions	
	        
			for (int c=0; c < nRandTargetConcepts; c++) {
							
				ShortFormProvider shortFormProvider = new SimpleShortFormProvider();
		        Set<OWLOntology> importsClosure = ontology.getImportsClosure();
	
		        BidirectionalShortFormProvider bidiShortFormProvider = 
		        		new BidirectionalShortFormProviderAdapter(manager, importsClosure, shortFormProvider);
	
		        ManchesterOWLSyntaxParser parser = OWLManager.createManchesterParser();
		        parser.setStringToParse((String) targets.get(c));
		        parser.setDefaultOntology(ontology);
		        
		        OWLEntityChecker entityChecker = new ShortFormEntityChecker(bidiShortFormProvider);
		        parser.setOWLEntityChecker(entityChecker);
		        
		        try {
		        	testConcepts[c] = parser.parseClassExpression();
		        }
		        catch (ParserException pe) {
					System.err.printf("Parsing problem with %s\n\n%s", targets.get(c), pe.getMessage());
					System.exit(1);
		        }
			}
		}
		else {
			testConcepts = RandomGenerator.generateTargetConcepts(this, nRandTargetConcepts);
		}
		
		// generate class expr for the complement classes of the test concepts
		negTestConcepts = new OWLClassExpression[testConcepts.length];
		for (int c=0; c<testConcepts.length; c++) 
			negTestConcepts[c] = dataFactory.getOWLObjectComplementOf(testConcepts[c]);
			
//		generate pos, neg instances for all test concept 
		generateExamples();
		
	}
	
	
	/**
	 * 
	 */
	void initKB() {
		
		System.out.println(iri);		
		
		try {
			manager = OWLManager.createOWLOntologyManager();
			dataFactory = manager.getOWLDataFactory();
	        ontology = manager.loadOntology(iri);
	
//			reasoner = ReasonerFactory.getInstance().createReasoner(ontology);
//	        reasonerFactory = new PelletReasonerFactory();
//			reasoner = reasonerFactory.createReasoner(ontology); 	

			
//	        OWLReasonerConfiguration config = new SimpleConfiguration();
	        reasoner = reasonerFactory.createReasoner(ontology);
	        
//			System.out.println("Precomputing inferences:");
//	        reasoner.precomputeInferences(InferenceType.CLASS_ASSERTIONS);     

	        
	        boolean consistent = reasoner.isConsistent();
	        System.out.printf("Consistency check: %s\n", consistent);
	        if (!consistent) {
	        	System.err.println("FATAL ERROR\n");
	        	System.exit(1);
	        }

        }
		catch(UnsupportedOperationException exception) {
			System.out.println("Unsupported reasoner operation.");
			System.exit(1);
		}
		catch (OWLOntologyCreationException e) {
			System.out.println("Could not load the ontology: " + e.getMessage());
			System.exit(1);
		}
		
		System.out.println("\nCLASSES");
		Stream<OWLClass> classList = ontology.classesInSignature();
		allConcepts = classList.toArray(size -> new OWLClass[size]); // new Java8 version
//		allConcepts = new OWLClass[classList.size()];

//		int c=0;
//        for(OWLClass cls : classList) {
//			if (!cls.isOWLNothing() && !cls.isAnonymous()) {
//				allConcepts[c++] = cls;
////				System.out.println(cls);
//			}	        		
//		}
        System.out.println("---------------------------- "+allConcepts.length);

		System.out.println("\nOBJECT Props\n");
//        Set<OWLObjectProperty> propList = ontology.getObjectPropertiesInSignature(); // deprecated in OWLAPI 5
        Stream<OWLObjectProperty> propList = ontology.objectPropertiesInSignature();
        allRoles = propList.toArray(size -> new OWLObjectProperty[size]); // new Java8 version

//        allRoles = propList.toArray(new OWLObjectProperty[propList.size()]);

//		allRoles = new OWLObjectProperty[propList.size()];
//		int op=0;
//        for(OWLObjectProperty prop : propList) {
//			if (!prop.isAnonymous()) {
//				allRoles[op++] = prop;
////				System.out.println(prop);
//			}	        		
//		}
        System.out.println("---------------------------- "+allRoles.length);

		System.out.println("\nDATA Props\n");
//        Set<OWLDataProperty> dataPropSet = ontology.getDataPropertiesInSignature();
        Stream<OWLDataProperty> dataPropSet = ontology.dataPropertiesInSignature();
//		allDataProperties = dataPropSet.toArray(new OWLDataProperty[dataPropSet.size()]);
		allDataProperties = dataPropSet.toArray(size -> new OWLDataProperty[size]);
//		System.out.println(dataPropSet);
        System.out.println("---------------------------- "+allDataProperties.length);
        
        
        System.out.println("\nINDIVIDUALS\n");
//        Set<OWLNamedIndividual> indList = ontology.getIndividualsInSignature();
        Stream<OWLNamedIndividual> indList = ontology.individualsInSignature();
//        allIndividuals = indList.toArray(new OWLNamedIndividual[indList.size()]);
        allIndividuals = indList.toArray(size -> new OWLNamedIndividual[size]);
        //        allIndividuals = new OWLNamedIndividual[indList.size()];
//        int i=0;
//        for(OWLNamedIndividual ind : indList) {
//			if (!ind.isAnonymous()) {
//				allIndividuals[i++] = ind;
////				System.out.println(ind);
//			}	        		
//		}
        System.out.println("---------------------------- "+allIndividuals.length);   

        System.out.println("\nCLASS DISJOINTNESS AXIOMS\n");
        long nDAxioms = ontology.getAxiomCount(AxiomType.DISJOINT_CLASSES,false);
        System.out.println("---------------------------- "+nDAxioms);   

        
        top = dataFactory.getOWLThing();

		
		System.out.println("\n\nKB LOADED. \n\n");	
	}
	
//	examples generation for all test concepts
	/**
	 * 
	 */
	void generateExamples() {
		

		
		 System.out.printf("\n--------------- creating GOLD STANDARD CLASSIFICATION ---------------\n"); 
		 System.out.printf("\nTARGET Concepts: %d\n", testConcepts.length); 
		 
		 classification = new int[testConcepts.length][allIndividuals.length];
		 ArrayList<Integer>[] positiveExs = new ArrayList[testConcepts.length];
		 ArrayList<Integer>[] negativeExs = new ArrayList[testConcepts.length];
		 ArrayList<Integer>[] unlabeledExs = new ArrayList[testConcepts.length];

		 
		 for(int c=0; c<testConcepts.length;c++) {
			 System.out.printf("concept: %d\t", c); 
			 
			 positiveExs[c] = new ArrayList<Integer>();
			 negativeExs[c] = new ArrayList<Integer>();
			 unlabeledExs[c] = new ArrayList<Integer>();

			 for(int i=0; i<allIndividuals.length; i++){
				 if(reasoner.isEntailed(dataFactory.getOWLClassAssertionAxiom(testConcepts[c], allIndividuals[i]))) {
					 positiveExs[c].add(i);
					 classification[c][i] = +1;
				 }
				 else if(reasoner.isEntailed(dataFactory.getOWLClassAssertionAxiom(negTestConcepts[c], allIndividuals[i]))) {
					 negativeExs[c].add(i);
					 classification[c][i] = -1;
				 }
				 else {
					 unlabeledExs[c].add(i);   // NEW 
					 classification[c][i] = 0; 
				 }
			 } 
			 System.out.printf("%10s %10s %10s\n", 
					 "pos:"+positiveExs[c].size(), "neg:"+negativeExs[c].size(), "und:"+unlabeledExs[c].size());	
		 } 
		 
		 
	}	
	
} // class

