# README #

DLFoil concept learner OWL2 
in Java using JFact+OWLAPI 5

### What is this repository for? ###

* Concept Learner in Description Logics (OWL2)
* 0.1

### How do I get set up? ###

* Summary of set up
* Configuration
 - Clone the repository via Git
 - Import the project as a Maven project
* Dependencies
 1. main: JFact 5.0.0, OWLAPI 5
 2. other: apache-commons - lang3, math3, configuration2, beanutils
 All the required libraries are imported through Maven 

### How to run?###
The  concept learner can be configured through an xml file,  where the user can specify the configuration of the algorithms and the design of the experiments (cross-validation/bootstrap)
An example of configuration file is reported below:

```xml
<?xml version="1.0" encoding="UTF-8" ?>

<config>

<kb>file:////C:/Users/Utente/Documents/Dottorato/Dataset/Dottorato/NTNcombined.owl</kb>

<!-- algorithm section -->
<algo>
	<!-- type of algo -->
	<!-- currently supported: DLFoil, TDT -->
	<type>DLFoil</type>
	<!-- num. of candidate refinements per turn -->
	<NC> 50 </NC>
</algo>

<!-- seed for random numbers generators -->
<seed> 1 </seed>

<!-- evaluation section (currently supported: .632 bootstrap -->
<evaluation>
	<!-- rand target concepts -->
 	<NRT> 2 </NRT> 
	<!-- num. of repeatitions -->
	<NF> 5 </NF>
	
<!-- target class expressions (Manchester syn) section. -->
	<targets>
<!--  Empty if the concepts are randomly generated  -->
	 	<target>
		Agent and (residentPlace some (City or subregion some (not GeographicLocation)))
		</target>
 	 	  <target> 
 		not (Woman) or knows some (not God) 
 		</target>

	</targets>

</evaluation>

</config>
```


