/**
 * 
 */
package it.uniba.di.lacam.ml;

import java.util.ArrayList;

import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLIndividual;

/**
 * @author NF
 *
 */
/**
 * @author nico
 *
 */
/**
 * @author nico
 *
 */
public class TDTClassificationModel extends InductiveClassificationModel {
	
	TDT model;
	
	public TDTClassificationModel(LProblem aProblem) {
		super(aProblem);
	}

	void learn(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {
		double numPos = posExs.size();
		double numNeg = negExs.size();
		double perPos = numPos/(numPos+numNeg);
		double perNeg = numNeg/(numPos+numNeg);
		model = TDTLearner.induceDLTree(problem, problem.top, posExs, negExs, undExs, problem.nCandSubConcepts, perPos, perNeg); 
	};

	int classify(OWLIndividual ind) {
		
		return classify(ind, model);
		
	}

	
	/**
	 * recursive down through the tree model
	 * @param ind
	 * @param tree
	 * @return
	 */
	private int classify(OWLIndividual ind, TDT tree) {
		
		OWLClassExpression rootClass = tree.getRoot();
		
		if (rootClass.equals(problem.top))
			return +1;
		if (rootClass.equals(problem.dataFactory.getOWLNothing()))
			return -1;
		
		int r1=0, r2=0;
		
		if (problem.reasoner.isEntailed(problem.dataFactory.getOWLClassAssertionAxiom(rootClass, ind)))
			r1 = classify(ind, tree.getPosSubTree());
	
		if (problem.reasoner.isEntailed(problem.dataFactory.getOWLClassAssertionAxiom(problem.dataFactory.getOWLObjectComplementOf(rootClass), ind)))
			r2 = classify(ind, tree.getNegSubTree());

		if (r1+r2==0) 
			return 0;
		else if (r1*r2==1) 
			return r1;
		else 
			return (r1!=0)? r1 : r2;
	}

} // class
