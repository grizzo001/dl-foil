/**
 * 
 */
package it.uniba.di.lacam.ml;

import java.util.ArrayList;

import org.semanticweb.owlapi.model.OWLIndividual;

/**
 * @author NF
 *
 */
public abstract class InductiveClassificationModel {
	
	protected LProblem problem;

	public InductiveClassificationModel(LProblem aProblem) {
		this.problem = aProblem;
	}
	
	abstract void learn(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs);

	abstract int classify(OWLIndividual ind);

} // class
