package it.uniba.di.lacam.ml;

import java.util.ArrayList;

import org.semanticweb.owlapi.model.OWLClassExpression;



public class TDT {

	private class DLNode {
		OWLClassExpression concept;		// node concept
		TDT pos; 			// positive decision subtree
		TDT neg; 			// negative decision subtree
		
		public DLNode(OWLClassExpression c) {
			concept = c;
			this.pos = this.neg = null; // node has no children
		}

//		public DLNode() {
//			concept = null;
////			this.pos = this.neg = null; // node has no children
//		}
		

		public String toString() {
			return this.concept.toString();
		}
		
	}
	

	private DLNode root; // Tree root
	
	
	public TDT () {
		this.root = null;
	}
	
	public TDT (OWLClassExpression c) {		
		this.root = new DLNode(c);
	}

	/**
	 * @param root the root to set
	 */
	public void setRoot(OWLClassExpression concept) {
		this.root = new DLNode(concept);
//		this.root.concept = concept;
	}

	/**
	 * @return the root
	 */
	public OWLClassExpression getRoot() {
		return root.concept;
	}


	public void setLSubTree(TDT subTree) {
		this.root.pos = subTree;
		
	}

	public void setRSubTree(TDT subTree) {
		
		this.root.neg = subTree;
		
	}
	
	public String toString() {
		if (root.pos == null && root.neg == null)
			return root.toString();
		else
			return root.concept.toString() + " ["+root.pos.toString()+" "+root.neg.toString()+"]";
	}

	public TDT getPosSubTree() {
		// TODO Auto-generated method stub
		return root.pos;
	}

	public TDT getNegSubTree() {
		// TODO Auto-generated method stub
		return root.neg;
	}

	
}
