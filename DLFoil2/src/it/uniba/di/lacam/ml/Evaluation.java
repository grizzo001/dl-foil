package it.uniba.di.lacam.ml;
/**
 * 
 */


import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;

import org.apache.commons.math3.stat.StatUtils;
import org.semanticweb.owlapi.dlsyntax.renderer.DLSyntaxObjectRenderer;
import org.semanticweb.owlapi.io.OWLObjectRenderer;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLNamedIndividual;

/**
 * To perform the evaluation of a supervised learning algorithm on a learning problem
 * 
 * @author NF 
 *
 */
public class Evaluation {	
	
	final int nOfFolds; 			//	number of folds
	final int nOfExs; 				//	number of examples	
	final int nOfPlaces; 			//	number of places in the mapping
	
	int exMapping[]; 				// array of the example indices 
	
	final int nPerFold;
	final int UNASSIGNED = -1;
	
	static final OWLObjectRenderer renderer = new DLSyntaxObjectRenderer(); 

	

	/**
	 * @param prob
	 * @param nFolds
	 * @param nExs
	 */
	public Evaluation(LProblem prob, int nFolds, int nExs) {


		nOfFolds =  nFolds;
		nOfExs = nExs;
		nPerFold = (int)Math.ceil((float)nExs/nFolds);
		nOfPlaces = nOfFolds*nPerFold;

		exMapping = new int[nOfPlaces];
		
		for (int p=0; p < nOfPlaces; p++) 
			exMapping[p] = UNASSIGNED;
		
		// random generation of a permutation of the integers in [0,nexs-1]
		for (int i=0; i<nExs; i++) {
			//	 find a random place for this i-th example index
			int rplace = Math.abs(RandomGenerator.generator.nextInt() % nExs); 
			while (exMapping[rplace] != UNASSIGNED) 
				rplace = (rplace + 1) % nExs;
			exMapping[rplace] = i;	
//			System.out.printf("Ex: %4d -->  Pos: %4d\n",i,rplace);
		}
		System.out.println("No of folds: "+nOfFolds);
		System.out.println("No of examples: "+nExs);
		System.out.println("No of places: "+nOfPlaces);
		System.out.println("No of examples per fold: "+nPerFold);		
	}



	/**
	 * create training set mappings of examples from other (nfolds-1) partitions
	 * @param foldNo
	 * @return
	 */
	int[] getTrainingExs(int foldNo) {
	
		int nTrainExs;
		if (foldNo<nOfFolds-1) { // for all but the last folds
			nTrainExs = (nOfFolds-1)*nPerFold - (nOfPlaces - nOfExs);
		} else { // last fold may contain fewer examples
			nTrainExs = (nOfFolds-1)*nPerFold;	
		}
		System.out.printf("#training examples %d - fold: %d\n\n",nTrainExs,foldNo);
		
		int[] trainingExs = new int[nTrainExs];
		java.util.Arrays.fill(trainingExs, -1);
		int IndTrEx = 0;
		for (int f = 0; f<nOfFolds; f++)
			if (foldNo != f)
				for (int t=f*nPerFold; t < (f+1)*nPerFold; t++) 
					if (exMapping[t] != UNASSIGNED) 
							{
								trainingExs[IndTrEx] = exMapping[t];
								IndTrEx++;
							}						
	
		if (IndTrEx != nTrainExs) System.exit(1); // incorrect number of tr. examples 
		return trainingExs;
	}




	/**
	 * @param fold
	 * @param n
	 * @return
	 */
	int getIndex(int fold, int n) {
		if (n < nPerFold && fold < nOfFolds)
			return exMapping[nPerFold*fold+n];
		else 
			return UNASSIGNED;	
	}



	
	
	/**
	 * @param prob
	 */
	public static void crossValidation(LProblem prob) {

		int nFolds = prob.nFolds;
		
		System.out.println(nFolds+"-fold CROSS VALIDATION Experiment on ontology: "+prob.urlOwlFile);				
		
		int nExs = prob.allIndividuals.length;		
		
		Evaluation cv = new Evaluation(prob,nFolds,nExs);

//		OWLClassExpression[] testConcepts = allConcepts;
		int nTestConcepts = prob.testConcepts.length;
		
		double[][] totMatchingRate 		= new double[nTestConcepts][nFolds]; 	// per OWLClassExpression per fold
		double[][] totCommissionRate 	= new double[nTestConcepts][nFolds]; 	// per OWLClassExpression per fold
		double[][] totOmissionRate 		= new double[nTestConcepts][nFolds]; 	// per OWLClassExpression per fold
		double[][] totInducedRate 		= new double[nTestConcepts][nFolds]; 	// per OWLClassExpression per fold
		double[][] totPrecision 		= new double[nTestConcepts][nFolds]; 	// per OWLClassExpression per fold
		double[][] totRecall 			= new double[nTestConcepts][nFolds]; 	// per OWLClassExpression per fold
				
		// main loop on the folds
		for (int f=0; f< nFolds; f++) {			
			
			double[] matchingNum 	= new double[nTestConcepts]; 	// per OWLClassExpression
			double[] commissionNum 	= new double[nTestConcepts]; 	// per OWLClassExpression
			double[] omissionNum 	= new double[nTestConcepts]; 	// per OWLClassExpression
			double[] inducedNum 	= new double[nTestConcepts]; 	// per OWLClassExpression
			double[] trueNum 		= new double[nTestConcepts]; 	// number of true examples per OWLClassExpression
			double[] foundNum 		= new double[nTestConcepts]; 	// number of examples retrieved as true per OWLClassExpression
			double[] hitNum 		= new double[nTestConcepts]; 	// number of hits per OWLClassExpression
			
			System.out.print("\n\nFold #"+f);
			System.out.println(" **************************************************************************************************");
			
			int[] trainingExs = cv.getTrainingExs(f);
			// test phase: test all examples in the f-th partition
	
			int indClassification = 1000000;
							
			for (int c=0; c < nTestConcepts; c++) {
				
				ArrayList<Integer> posExs = new ArrayList<Integer>();
				ArrayList<Integer> negExs = new ArrayList<Integer>();
				ArrayList<Integer> undExs = new ArrayList<Integer>();								
				
				System.out.printf("--------- Target Concept #%d \n%s",c,renderer.render(prob.testConcepts[c]));
				for (int e=0; e<trainingExs.length; e++)
					if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(prob.testConcepts[c],prob.allIndividuals[trainingExs[e]])))
						posExs.add(trainingExs[e]);
					else if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(prob.negTestConcepts[c], prob.allIndividuals[trainingExs[e]])))
						negExs.add(trainingExs[e]);
					else
						undExs.add(trainingExs[e]);
				
				System.out.printf("\n\nLearning problem prepared.\n");	
				OWLClassExpression inducedConcept = DLFoil.induceConcept(prob, posExs,negExs,undExs);				

				System.out.printf("induced for target #%d:\n\n%s \n\n\n",c, renderer.render(prob.testConcepts[c]));
				
				for (int te=0; te < cv.nPerFold; te++ ) { 
					
					int indTestEx = cv.getIndex(f,te);
					if (indTestEx != cv.UNASSIGNED) {
					
						indClassification = classifyExample(prob, te, inducedConcept);
						if (indClassification == 1)
							++foundNum[c];
						
						int rclass = 0;
						if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(prob.testConcepts[c], prob.allIndividuals[indTestEx]))) {
							rclass = +1;
							++trueNum[c];
						}
						else {
							if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(prob.negTestConcepts[c],prob.allIndividuals[indTestEx]))) 
								rclass = -1;
						}
						
						if (indClassification == rclass) { 
							++matchingNum[c];
							if (rclass==1) 
								++hitNum[c];
						}
						else if (Math.abs(indClassification - rclass)>1) { 
							++commissionNum[c];
						}
						else if (rclass != 0) {
							++omissionNum[c];
						}	
						else {
							++inducedNum[c];
						}
					} // if (indTestEx != cv.UNASSIGNED) {
				} // for te 
			} // for c - inPartition loop

			System.out.println("\n\n |||||||||||||||||||||||||||||||||||||||||||||||||||||||||| OUTCOMES FOLD #"+f);
			System.out.printf("\n%10s %10s %10s %10s %10s %10s %10s\n", 
					"TargetC#",  "matching", "commission", "omission", "induction", "precision", "recall");
			
			double[] precision = new double[nTestConcepts];
			double[] recall = new double[nTestConcepts];
			double nCases = (f==cv.nOfFolds-1) ? cv.nPerFold- (cv.nOfPlaces - cv.nOfExs): cv.nPerFold;
			
			for (int c=0; c < nTestConcepts; c++) {
				
				totMatchingRate[c][f] = matchingNum[c]/nCases; 
				totCommissionRate[c][f] = commissionNum[c]/nCases; 
				totOmissionRate[c][f] = omissionNum[c]/nCases;  
				totInducedRate[c][f] = inducedNum[c]/nCases;
				
				totPrecision[c][f] = precision[c] = (hitNum[c]+1)/(foundNum[c]+1);
				totRecall[c][f] = recall[c] = (hitNum[c]+1)/(trueNum[c]+1);
					
				System.out.printf("%9d. %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n", c, 
							matchingNum[c]/nCases, commissionNum[c]/nCases, 
							omissionNum[c]/nCases, inducedNum[c]/nCases, 
							precision[c], recall[c] );
				
			}
			System.out.println("----------------------------------------------------------------------------------------------");
			System.out.printf("%10s %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\n", "AVERAGES", 
					StatUtils.mean(matchingNum)/nCases, StatUtils.mean(commissionNum)/nCases, 
					StatUtils.mean(omissionNum)/nCases, StatUtils.mean(inducedNum)/nCases, 
					StatUtils.mean(precision), StatUtils.mean(recall));

		} // for f - fold look		
		
		
		System.out.println("\n\n\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ OVERALL OUTCOMES");
		System.out.printf("\n%10s %10s %10s %10s %10s %10s %10s\n", "TargetC#",  "matching", "commission", "omission", "induction", "precision", "recall");
		
		double accMatchingAvgs = 0;
		double accCommissionAvgs = 0;
		double accOmissionAvgs = 0;
		double accInductionAvgs = 0;
		double accPrecisionAvgs = 0;
		double accRecallAvgs = 0;
		
		for (int c=0; c < nTestConcepts; c++) {
			double AvgMatching = StatUtils.mean(totMatchingRate[c]);
			double AvgCommission = StatUtils.mean(totCommissionRate[c]);
			double avgOmission = StatUtils.mean(totOmissionRate[c]);
			double avgInduction = StatUtils.mean(totInducedRate[c]);
			
			double avgPrecision = StatUtils.mean(totPrecision[c]);
			double avgRecall = StatUtils.mean(totRecall[c]);
			
			System.out.printf("%10d %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f \n", 
					c, AvgMatching, AvgCommission, avgOmission, avgInduction, avgPrecision, avgRecall);
			accMatchingAvgs += AvgMatching;
			accCommissionAvgs += AvgCommission;
			accOmissionAvgs += avgOmission;
			accInductionAvgs += avgInduction;
			accPrecisionAvgs += avgPrecision;
			accRecallAvgs += avgRecall;
		}
		System.out.println("----------------------------------------------------------------------------------------------");
		double matchingAvg 		= accMatchingAvgs/nTestConcepts;
		double commissionAvg 	= accCommissionAvgs/nTestConcepts;
		double omissionAvg 		= accOmissionAvgs/nTestConcepts;
		double inductionAvg 	= accInductionAvgs/nTestConcepts;
		double precisionAvg 	= accPrecisionAvgs/nTestConcepts;
		double recallAvg 		= accRecallAvgs/nTestConcepts;		
		
		double theFMeasure = 2*precisionAvg*recallAvg / (precisionAvg + recallAvg);
		System.out.printf("%10s %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f \t F-measure: %10.3f\n", "AVERAGES", 
				matchingAvg, commissionAvg, omissionAvg, inductionAvg, precisionAvg, recallAvg, theFMeasure);
	} // CV


	
	
	/**
	 * inductive classification of a test example w.r.t. (the model for) a test concept
	 * 
	 * @param prob
	 * @param indTestEx
	 * @param tConcept
	 * @return membership value as an int
	 */
	static int classifyExample(LProblem prob, int indTestEx, OWLClassExpression tConcept) {		
//	TODO modifica per generalizzare rispetto al modello appreso 	
		if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(tConcept,prob.allIndividuals[indTestEx])))
			return +1;
		else if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(prob.dataFactory.getOWLObjectComplementOf(tConcept),prob.allIndividuals[indTestEx])))
			return -1;
		else 
			return 0;
		
	}





	

	/**
	 * @param prob
	 */
	public static void bootstrap(LProblem prob) {
		
		int nFolds = prob.nFolds;
		System.out.printf("\n\n %d BOOTSTRAP Experiments on ontology: %s", nFolds, prob.urlOwlFile);		
				
//		OWLClass[] testConcepts = allConcepts;
		int nOfConcepts = prob.testConcepts.length;
		
		double[][] totMatchingRate = new double[nOfConcepts][nFolds]; // per OWLClass per fold
		double[][] totCommissionRate = new double[nOfConcepts][nFolds]; // per OWLClass per fold
		double[][] totOmissionRate = new double[nOfConcepts][nFolds]; // per OWLClass per fold
		double[][] totInducedRate = new double[nOfConcepts][nFolds]; // per OWLClass per fold

		double[] matchingStdDev = new double[nOfConcepts]; // per OWLClass per fold
		double[] commissionStdDev = new double[nOfConcepts]; // per OWLClass per fold
		double[] omissionStdDev = new double[nOfConcepts]; // per OWLClass per fold
		double[] inducedStdDev = new double[nOfConcepts]; // per OWLClass per fold		
		
		// main loop on the folds
		int[] ntestExs = new int[nFolds];
		for (int f=0; f < nFolds; f++) {			
			
			int[] matchingNum = new int[nOfConcepts]; // per OWLClass
			int[] commissionNum = new int[nOfConcepts]; // per OWLClass
			int[] omissionNum = new int[nOfConcepts]; // per OWLClass
			int[] inducedNum = new int[nOfConcepts]; // per OWLClass			
						
			System.out.printf("\n\n Bootstrap Fold #%d **************************************************************************************************\n",f);
			
			// prepare training and test sets
			Set<Integer> trainingExsSet = new HashSet<Integer>();
			Set<Integer> testingExsSet = new HashSet<Integer>();

			boolean nonTrivial; // must contain pos exs for all classes
			do {
				nonTrivial = true;
				int numPos = 0, numNeg = 0; // # n. of pos & neg training exs

				for (int ii=0; ii < prob.allIndividuals.length; ii++) {
					trainingExsSet.add(RandomGenerator.generator.nextInt(prob.allIndividuals.length));
				}
				for (int c=0; c < prob.testConcepts.length && nonTrivial; c++) {
					numPos = 0; numNeg = 0;
					for (Integer tIndex: trainingExsSet) 
						if (prob.classification[c][tIndex] == +1)
							++numPos;
						else if (prob.classification[c][tIndex] == -1)
							++numNeg;
					nonTrivial = (numPos > 0);
				}
				System.out.printf("Tot:%d Training:%d [Pos:%d\t Neg:%d\t Und:%d]\n\n", 
						prob.allIndividuals.length, trainingExsSet.size(), 
						numPos, numNeg, trainingExsSet.size()-numPos-numNeg);
			} while (!nonTrivial || (prob.allIndividuals.length-trainingExsSet.size()<1));
			
			for (int e=0; e < prob.allIndividuals.length; e++) 
				if (!trainingExsSet.contains(e)) 
					testingExsSet.add(e);

			Integer[] trainingExs = trainingExsSet.toArray(new Integer[trainingExsSet.size()]);
			Integer[] testExs = testingExsSet.toArray(new Integer[testingExsSet.size()]);
			ntestExs[f] = testExs.length;				
			
		
			System.out.printf("\nTraining Exs #%d\t Test Exs #%d\n",trainingExs.length,testExs.length);
			
		
			// training phase: using all examples but those in the f-th partition
			System.out.println("Training is starting...");
			
//			OWLClassExpression[] inducedConcepts = new OWLClassExpression[nOfConcepts];
//			InductiveClassificationModel[] inducedModel = new CEClassificationModel[nOfConcepts];
//			InductiveClassificationModel[] inducedModel = new TDTClassificationModel[nOfConcepts];
			InductiveClassificationModel[] inducedModel = new InductiveClassificationModel[nOfConcepts];
			
			
			
			for (int c = 0; c < prob.testConcepts.length; c++) {
				
//				inducedModel[c] = new CEClassificationModel(prob);
				try {
					inducedModel[c] = (InductiveClassificationModel) prob.algoType.getDeclaredConstructor(LProblem.class).newInstance(prob);
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | NoSuchMethodException | SecurityException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
								
				ArrayList<Integer> posExs = new ArrayList<Integer>();
				ArrayList<Integer> negExs = new ArrayList<Integer>();
				ArrayList<Integer> undExs = new ArrayList<Integer>();								
				
				System.out.printf("FOLD %d --------- Target Concept #%d \n%s", f, c, renderer.render(prob.testConcepts[c]));

//				separate training set
				for (int e=0; e<trainingExs.length; e++)
					if (prob.classification[c][trainingExs[e]] == +1)
						posExs.add(trainingExs[e]);
					else if (prob.classification[c][trainingExs[e]] == -1)
						negExs.add(trainingExs[e]);
					else
						undExs.add(trainingExs[e]);
				System.out.printf("\n\nLearning problem prepared.\n");	
//				inducedConcepts[c] = DLFoil.induceConcept(prob, posExs,negExs,undExs);
				inducedModel[c].learn(posExs,negExs,undExs);
				
				System.out.printf(">>> induced for target #%d:\n%s\n \n\n\n",c, renderer.render(prob.testConcepts[c]));
			}	
			
//			OWLClassExpression[] negInducedConcepts = new OWLClassExpression[nOfConcepts];
//			for (int c=0; c<prob.testConcepts.length; ++c) 
//				negInducedConcepts[c] = prob.dataFactory.getOWLObjectComplementOf(inducedConcepts[c]);
			
			System.out.println("End of Training.\n\n");
			
			

			// test phase: test all examples in the f-th partition
			
			System.out.println("Test is starting...");
			
			System.out.print("\n");
			
			for (int te=0; te < testExs.length; te++) { 
				
				int indTestEx = testExs[te];
				OWLNamedIndividual testInd = prob.allIndividuals[indTestEx];
				
				System.out.print("\nFold #"+f);
				System.out.println(" --- Classifying Ex. " + (te+1) +"/"+testExs.length +" [" + indTestEx + "] " + renderer.render(testInd));
								
				int[] indClassifications = new int[prob.testConcepts.length];				
				
				// inductive classification
				for (int c=0; c<prob.testConcepts.length; ++c) { 
					indClassifications[c] = inducedModel[c].classify(testInd);		
//					if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(inducedConcepts[c],testInd))) 
//						indClassifications[c] = +1;
//					else if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(negInducedConcepts[c],testInd))) 
//						indClassifications[c] = -1;
//					else
//						indClassifications[c] = 0;
				}
//				System.out.print("\n");

//				System.out.printf("\n%10s %10s %10s", "TargetC#",  "I.CLASS", "D.CLASS");
				
				for (int c=0; c < nOfConcepts; c++) {
					
					int rclass = prob.classification[c][indTestEx]; // reasoner-determined class
					
					if (indClassifications[c] == rclass) { 
						++matchingNum[c];							
					}
					else if (Math.abs(indClassifications[c] - rclass)>1) { 
						System.out.printf("\t %d.committed(%d)",c,rclass);
						++commissionNum[c];
					}
					else if (indClassifications[c] == 0 && rclass != 0) {
						System.out.printf("\t %d.omitted(%d)",c,rclass);
						++omissionNum[c];
					}	
					else {
						System.out.printf("\t %d.induced(%d)",c,rclass);
						++inducedNum[c];
					}
				}
			} // for t - inPartition loop
			
			
			System.out.println("\n\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> OUTCOMES FOLD #"+f);
			System.out.printf("\n%10s %10s %10s %10s %10s\n", "TargetC#",  "matching", "commission", "omission", "induction");
			for (int c=0; c < nOfConcepts; c++) {
				totMatchingRate[c][f] = matchingNum[c]/(double)ntestExs[f]; 
				totCommissionRate[c][f] = commissionNum[c]/(double)ntestExs[f]; 
				totOmissionRate[c][f] = omissionNum[c]/(double)ntestExs[f];  
				totInducedRate[c][f] = inducedNum[c]/(double)ntestExs[f];
				System.out.printf("%10d %10.3f %10.3f %10.3f %10.3f \n", c, totMatchingRate[c][f], totCommissionRate[c][f], totOmissionRate[c][f], totInducedRate[c][f]);
				
			}
		} // for f - fold look
		
		System.out.println("\n\n\n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ OVERALL OUTCOMES");
		System.out.printf("\n%10s %10s %10s %10s %10s\n", "TargetC#",  "matching %", "commission %", "omission %", "induction %");
		
		double accMatchingAvgs = 0;
		double accCommissionAvgs = 0;
		double accOmissionAvgs = 0;
		double accInductionAvgs = 0;
		for (int c=0; c < nOfConcepts; c++) {
			double AvgMatching = StatUtils.mean(totMatchingRate[c]);
			double AvgCommission = StatUtils.mean(totCommissionRate[c]);
			double avgOmission = StatUtils.mean(totOmissionRate[c]);
			double avgInduction = StatUtils.mean(totInducedRate[c]);
			
			System.out.printf("%10d %10.2f %10.2f %10.2f %10.2f \n", c, 
					AvgMatching*100, AvgCommission*100, avgOmission*100, avgInduction*100);
			accMatchingAvgs += AvgMatching;
			accCommissionAvgs += AvgCommission;
			accOmissionAvgs += avgOmission;
			accInductionAvgs += avgInduction;
			
			matchingStdDev[c] = stdDeviation(totMatchingRate[c]);
			commissionStdDev[c] = stdDeviation(totCommissionRate[c]);
			omissionStdDev[c] = stdDeviation(totOmissionRate[c]);
			inducedStdDev[c] = stdDeviation(totInducedRate[c]);
		}
		

		
		System.out.println("----------------------------------------------------------------------------------------------");
		double matchingAvg 		= accMatchingAvgs/nOfConcepts;
		double commissionAvg 	= accCommissionAvgs/nOfConcepts;
		double omissionAvg 		= accOmissionAvgs/nOfConcepts;
		double inductionAvg 	= accInductionAvgs/nOfConcepts;
		
		System.out.printf("%10s %10.2f %10.2f %10.2f %10.2f \n", "avg Values", 
							matchingAvg*100, commissionAvg*100, omissionAvg*100, inductionAvg*100);
		System.out.printf("%10s %10.2f %10.2f %10.2f %10.2f \n", "avg StdDev", 
				StatUtils.mean(matchingStdDev)*100, StatUtils.mean(commissionStdDev)*100, StatUtils.mean(omissionStdDev)*100, StatUtils.mean(inducedStdDev)*100);
	} // bootstrap
	
	

	
	

	static double stdDeviation(double[] population) {
		return Math.sqrt(StatUtils.populationVariance(population));
	}
	
} // class
