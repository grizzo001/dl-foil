/**
 * 
 */
package it.uniba.di.lacam.ml;

import java.util.ArrayList;

import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLIndividual;

/**
 * @author NF
 *
 */
public class CEClassificationModel extends InductiveClassificationModel {
	
	OWLClassExpression model;
	
	public CEClassificationModel(LProblem aProblem) {
		super(aProblem);
	}

	void learn(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {
		model = DLFoil.induceConcept(problem, posExs, negExs, undExs); 
	};

	int classify(OWLIndividual ind) {
		if (problem.reasoner.isEntailed(problem.dataFactory.getOWLClassAssertionAxiom(model,ind))) 
			return +1;
		else if (problem.reasoner.isEntailed(problem.dataFactory.getOWLClassAssertionAxiom(model.getComplementNNF(),ind))) 
			return -1;
		else
			return 0;
	}

} // class
