package it.uniba.di.lacam.ml;


public class DLFoilTest {
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
	
		LProblem problem = new LProblem(args[0]);

		RandomGenerator.init(problem.seed);
		
		Evaluation.bootstrap(problem); // run an experiment session
		
		System.out.print("\n\nEND: "+problem.urlOwlFile);
		System.out.printf("\t Seed: %d", problem.seed);


	} // main
	
	
	
} // class DLFoilTest